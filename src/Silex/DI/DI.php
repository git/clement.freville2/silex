<?php

declare(strict_types=1);

namespace Silex\DI;

use Silex\Gateway\NewsGateway;
use Silex\Gateway\CommentGateway;
use PDO;
use Silex\Gateway\UserGateway;
use Silex\Router\Router;
use Silex\Security\Security;

class DI
{
    private Router $router;
    private ?PDO $pdo = null;
    private ?NewsGateway $newsGateway = null;
    private ?CommentGateway $commentGateway = null;
    private ?UserGateway $userGateway = null;
    private ?Security $security = null;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function getRouter(): Router
    {
        return $this->router;
    }

    public function getNewsGateway(): NewsGateway
    {
        if ($this->newsGateway === null) {
            $this->newsGateway = new NewsGateway($this->getPDO());
        }
        return $this->newsGateway;
    }

    public function getUserGateway(): UserGateway
    {
        if ($this->userGateway === null) {
            $this->userGateway = new UserGateway($this->getPDO());
        }
        return $this->userGateway;
    }

    public function getSecurity(): Security
    {
        if ($this->security === null) {
            session_start();
            $this->security = new Security($this->getUserGateway(), $_SESSION);
        }
        return $this->security;
    }

    public function getCommentGateway(): CommentGateway
    {
        if ($this->commentGateway === null) {
            $this->commentGateway = new CommentGateway($this->getPDO());
        }
        return $this->commentGateway;
    }

    private function getPDO(): PDO
    {
        if ($this->pdo === null) {
            $this->pdo = new PDO(sprintf('mysql:host=%s;dbname=%s', DB_HOST, DB_DATABASE), DB_USER, DB_PASSWORD);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $this->pdo;
    }
}
