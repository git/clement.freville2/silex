<?php

declare(strict_types=1);

namespace Silex\Gateway;

use PDOException;

final class UniqueViolation
{
    public static function isUniqueViolation(PDOException $ex): bool
    {
        return $ex->errorInfo[1] === 1062; // Mysql
    }
}
