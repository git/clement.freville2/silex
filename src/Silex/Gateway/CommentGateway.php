<?php

declare(strict_types=1);

namespace Silex\Gateway;

use DateTime;
use PDO;
use Silex\Model\Comment;

class CommentGateway
{
	private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function insert(Comment $comment): bool
    {
        $req = $this->pdo->prepare('INSERT INTO comment (news_id, content, author_id, author_name) VALUES (:news_id, :content, :author_id, :author_name);');
        $req->bindValue(':news_id', $comment->getNewsId(), PDO::PARAM_INT);
        $req->bindValue(':content', $comment->getContent());
        $req->bindValue(':author_id', $comment->getAuthorId(), $comment->getAuthorId() !== null ? PDO::PARAM_INT : PDO::PARAM_NULL);
        if ($comment->getAuthorId() === null) {
            $req->bindValue(':author_name', $comment->getAuthorName());
        } else {
            $req->bindValue(':author_name', null, PDO::PARAM_NULL);
        }
        $req->execute();
        $comment->setId(intval($this->pdo->lastInsertId()));
        return true;
    }

    public function update(Comment $comment): bool
    {
        $req = $this->pdo->prepare('UPDATE comment SET news_id = :news_id, content = :content, author_id = :author_id WHERE id_comment = :id_comment;');
        $req->execute(['news_id' => $comment->getNewsId(), 'content' => $comment->getContent(), 'author_id' => $comment->getAuthorId(), 'id_comment' => $comment->getId()]);
        return true;
    }

    public function delete(Comment $comment): void
    {
        $req = $this->pdo->prepare('DELETE FROM comment WHERE id_comment = :id_comment;');
        $req->execute(['id_comment' => $comment->getId()]);
    }

    /**
     * @return Comment[]
     */
    public function getByNewsId(int $id): array
    {
        $req = $this->pdo->prepare('SELECT c.*, COALESCE(c.author_name, u.login) author_name FROM comment c LEFT JOIN registered_user u ON u.id_user = c.author_id WHERE c.news_id = :id ORDER BY c.publication_date ASC');
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        if (!$req->execute()) {
            return [];
        }
        $comments = [];
        while ($data = $req->fetch()) {
            $comments[] = $this->createComment($data);
        }
        return $comments;
    }

    public function getCommentNumber(): int
    {
        $req = $this->pdo->prepare('SELECT COUNT(*) FROM comment');
        $req->execute();
        $data = $req->fetch();
        return intval($data[0]);
    }

    public function getCommentNumberFromUser(int $id): int
    {
        $req = $this->pdo->prepare('SELECT COUNT(*) FROM comment WHERE author_id = :id');
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        $req->execute();
        $data = $req->fetch();
        return intval($data[0]);
    }

    private function createComment(array $data): Comment
    {
        $comment = new Comment(
            intval($data['id_comment']),
            intval($data['news_id']),
            DateTime::createFromFormat('Y-m-d H:i:s', $data['publication_date']),
            $data['content'],
            intval($data['author_id'])
        );
        if (isset($data['author_name'])) {
            $comment->setAuthorName($data['author_name']);
        }
        return $comment;
    }
}
