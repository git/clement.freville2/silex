<?php

declare(strict_types=1);

namespace Silex\Gateway;

use DateTime;
use PDO;
use Silex\Model\News;

class NewsGateway
{
    private const EXCERPT_LENGTH = 180;

    private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function insert(News $news): void
    {
        $req = $this->pdo->prepare('INSERT INTO news (title, slug, content, author_id) VALUES (:title, :slug, :content, :author_id);');
        $req->execute(['title' => $news->getTitle(), 'slug' => $news->getSlug(), 'content' => $news->getContent(), 'author_id' => $news->getAuthorId()]);
        $news->setId(intval($this->pdo->lastInsertId()));
    }

    public function update(News $news): void
    {
        $req = $this->pdo->prepare('UPDATE news SET title = :title, slug = :slug, content = :content, author_id = :author_id WHERE id_news = :id_news;');
        $req->execute(['title' => $news->getTitle(), 'slug' => $news->getSlug(), 'content' => $news->getContent(), 'author_id' => $news->getAuthorId(), 'id_news' => $news->getId()]);
    }

    public function delete(News $news): void
    {
        $req = $this->pdo->prepare('DELETE FROM news WHERE id_news = :id_news;');
        $req->execute(['id_news' => $news->getId()]);
    }

    /**
     * @return News[]
     */
    public function getPaginatedRecentNews(int $page = 1, int $limit = 10): array
    {
        $req = $this->pdo->prepare('SELECT id_news, title, LEFT(content, ' . self::EXCERPT_LENGTH . ') content, publication_date, author_id FROM news ORDER BY publication_date DESC LIMIT :limit OFFSET :offset;');
        $req->bindValue('limit', $limit, PDO::PARAM_INT);
        $req->bindValue('offset', ($page - 1) * $limit, PDO::PARAM_INT);
        if (!$req->execute()) {
            return [];
        }
        $news = [];
        while ($data = $req->fetch()) {
            $news[] = $this->createNews($data);
        }
        return $news;
    }

    public function getCount(): int
    {
        $req = $this->pdo->query('SELECT COUNT(*) nb FROM news;');
        if ($req === false) {
            return 0;
        }
        return intval($req->fetch()['nb']);
    }

    public function getById(int $id): ?News
    {
        $req = $this->pdo->prepare('SELECT * FROM news WHERE id_news=:id;');
        $req->bindValue(':id', $id, PDO::PARAM_INT);
        if (!$req->execute()) {
            return null;
        }
        $data = $req->fetch();
        return $data === false ? null : $this->createNews($data);
    }

    /**
     * @return News[]
     */
    public function getLike(string $dateDeb, string $dateFin,int $page = 1, int $limit = 10): array {
        $req = $this->pdo->prepare('SELECT * FROM news WHERE DATE_FORMAT(publication_date, "%Y-%m-%d") BETWEEN DATE_FORMAT(:dateDeb, "%Y-%m-%d") AND DATE_FORMAT(:dateFin, "%Y-%m-%d") ORDER BY publication_date LIMIT :limit OFFSET :offset;');
        $req->bindValue(':dateDeb', $dateDeb, PDO::PARAM_STR);
        $req->bindValue(':dateFin', $dateFin, PDO::PARAM_STR);
        $req->bindValue('limit', $limit, PDO::PARAM_INT);
        $req->bindValue('offset', ($page - 1) * $limit, PDO::PARAM_INT);
        if (!$req->execute()) {
            return [];
        }
        $news = [];
        while ($data = $req->fetch()) {
            $news[] = $this->createNews($data);
        }
        return $news;
    }

    private function createNews(array $data): News
    {
        return new News(intval($data['id_news']), $data['title'], $data['content'], DateTime::createFromFormat('Y-m-d H:i:s', $data['publication_date']), intval($data['author_id']));
    }
}
