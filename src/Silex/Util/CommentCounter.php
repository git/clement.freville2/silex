<?php

namespace Silex\Util;

const COMMENTS = 'comments';

final class CommentCounter
{
    public static function incrementCommentCounter()
    {
        $c = $_COOKIE[COMMENTS] ?? '0';
        if (!is_numeric($c)) {
            $c = '0';
        }
        setcookie(COMMENTS, intval($c) + 1, time() + 60*60*24*30, '/');
    }
}
