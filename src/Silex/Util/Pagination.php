<?php

declare(strict_types=1);

namespace Silex\Util;

final class Pagination
{
    public static function getNbPages(int $nbItems, int $perPage): int
    {
        return intval(ceil($nbItems / $perPage));
    }
}
