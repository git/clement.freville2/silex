<?php

declare(strict_types=1);

namespace Silex\Controller;

use Silex\DI\DI;
use Silex\Http\HttpResponse;
use Silex\Router\Route;

class FrontController
{
    private ?Route $route;

    public function __construct(?Route $route)
    {
        $this->route = $route;
    }

    public function run(DI $di): HttpResponse
    {
        if ($this->route === null) {
            return new HttpResponse(404, 'errors', ['errors' => ['Route not found']]);
        }
        if ($this->route->getController() instanceof AdminController
            && ($di->getSecurity()->getCurrentUser() === null || !$di->getSecurity()->getCurrentUser()->isAdmin())) {
            HttpResponse::redirect($di->getRouter()->url('login'));
        }
        return $this->route->call($di);
    }
}
