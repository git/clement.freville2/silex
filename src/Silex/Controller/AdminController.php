<?php

declare(strict_types=1);

namespace Silex\Controller;

use DateTime;
use Silex\DI\DI;
use Silex\Http\HttpResponse;
use Silex\Model\News;
use Silex\Validation\NewsValidation;

class AdminController
{
    public function publish(DI $di): HttpResponse
    {
        $errors = [];
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && NewsValidation::isValidNews($_POST, $errors)) {
            $news = new News(-1, $_POST['title'], $_POST['content'], new DateTime(), $di->getSecurity()->getCurrentUserId());
            $di->getNewsGateway()->insert($news);
            HttpResponse::redirect($di->getRouter()->url($news->getSlugRedirect()));
        }
        $news = new News(-1, '', '', new DateTime(), $di->getSecurity()->getCurrentUserId());
        return HttpResponse::found('edit', ['news' => $news, 'errors' => $errors]);
    }

    public function edit(DI $di, array $params): HttpResponse
    {
        $news = $di->getNewsGateway()->getById(intval($params['id']));
        if ($news === null) {
            return new HttpResponse(404, 'errors', ['errors' => ['Unknown news']]);
        }
        $errors = [];
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && NewsValidation::isValidNews($_POST, $errors)) {
            $news = new News($news->getId(), $_POST['title'], $_POST['content'], $news->getPublicationDate(), $news->getAuthorId());
            $di->getNewsGateway()->update($news);
            HttpResponse::redirect($di->getRouter()->url($news->getSlugRedirect()));
        }
        return HttpResponse::found('edit', ['news' => $news, 'errors' => $errors]);
    }

    public function delete(DI $di, array $params): HttpResponse
    {
        $news = $di->getNewsGateway()->getById(intval($params['id']));
        if ($news === null) {
            return new HttpResponse(404, 'errors', ['errors' => ['Unknown news']]);
        }
        $di->getNewsGateway()->delete($news);
        HttpResponse::redirect($di->getRouter()->url(''));
        exit();
    }
}
