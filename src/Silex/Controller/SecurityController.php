<?php

declare(strict_types=1);

namespace Silex\Controller;

use Silex\DI\DI;
use Silex\Http\HttpResponse;
use Silex\Model\User;
use Silex\Validation\UserValidation;

class SecurityController
{
    public function login(DI $di): HttpResponse
    {
        $errors = [];
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && UserValidation::isValidLogin($_POST,$errors)) {
            $success = $di->getSecurity()->initLogin($_POST['login'], $_POST['password']);
            if ($success) {
                HttpResponse::redirect($di->getRouter()->url(''));
            } else {
                $errors[] = 'Login or password invalid';
            }
        }
        return HttpResponse::found('login', ['errors' => $errors]);
    }

    public function register(DI $di): HttpResponse
    {
        $errors = [];
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && UserValidation::isValidUser($_POST,$errors)) {
            $user = $di->getSecurity()->register(User::fromRawPassword($_POST['login'], $_POST['password']));
            if ($user !== null) {
                HttpResponse::redirect($di->getRouter()->url(''));
            }
            if($user === null){
                $errors[] = 'Login is already taken';
            }
        }
        return HttpResponse::found('register', ['errors' => $errors]);
    }

    public function logout(DI $di): void
    {
        $di->getSecurity()->logout();
        HttpResponse::redirect($di->getRouter()->url(''));
    }
}
