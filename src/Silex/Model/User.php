<?php

declare(strict_types=1);

namespace Silex\Model;

class User
{
    private int $id_user;
    private string $login;
    private string $password;
    private int $role;

    public static function fromRawPassword(string $login, string $password, int $role = 0): User
    {
        $user = new User();
        $user->login = $login;
        $user->password = password_hash($password, PASSWORD_DEFAULT);
        $user->role = $role;
        return $user;
    }

    public function getId(): int
    {
        return $this->id_user;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPasswordHash(): string
    {
        return $this->password;
    }

    public function getRole(): int
    {
        return $this->role;
    }

    public function isAdmin(): bool
    {
        return $this->role >= 1;
    }

    public function setId(int $id)
    {
        $this->id_user = $id;
    }
}
