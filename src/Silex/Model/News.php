<?php

declare(strict_types=1);

namespace Silex\Model;

use DateTime;

class News
{
    private int $id;
    private string $title;
    private string $content;
    private DateTime $publicationDate;
    private int $authorId;

    public function __construct(int $id, string $title, string $content, DateTime $publicationDate, int $authorId)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->publicationDate = $publicationDate;
        $this->authorId = $authorId;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->title)));
    }

    public function getSlugRedirect(): string 
    {
        return 'news/' . $this->getSlug() . '-'. $this->getId();
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getPublicationDate(): DateTime
    {
        return $this->publicationDate;
    }

    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
