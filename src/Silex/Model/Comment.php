<?php

declare(strict_types=1);

namespace Silex\Model;

use DateTime;

class Comment
{
	private int $idComment;
	private int $newsId;
	private DateTime $publicationDate;
	private string $content;
	private ?int $authorId = null;
    private string $authorName;

	public function __construct(int $idComment, int $newsId, DateTime $publicationDate, string $content)
    {
        $this->idComment = $idComment;
        $this->newsId = $newsId;
        $this->publicationDate = $publicationDate;
        $this->content = $content;
    }

    public function getId(): int
    {
        return $this->idComment;
    }

    public function getNewsId(): int
    {
    	return $this->newsId;
    }

    public function getPublicationDate(): DateTime
    {
        return $this->publicationDate;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getAuthorId(): ?int
    {
    	return $this->authorId;
    }

    public function getAuthorName(): string
    {
    	return $this->authorName;
    }

    public function setId(int $id): void
    {
        $this->idComment = $id;
    }

    public function setAuthorName(string $authorName): void
    {
        $this->authorName = $authorName;
    }

    public function setAuthor(User $author): void
    {
        $this->authorId = $author->getId();
        $this->authorName = $author->getLogin();
    }
}
