<?php

namespace Silex\Validation;

final class CommentValidation
{
    public static function isValidComment(array &$post, bool $requiresName, array &$errors): bool
    {
        if ($requiresName) {
            UserValidation::isValidName($post, $errors, 'name');
        }
        if (empty($post['content'])) {
            $errors[] = 'Empty message';
        }
        $post['content'] = htmlspecialchars($post['content']);
        return empty($errors);
    }
}
