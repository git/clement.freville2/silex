<?php

namespace Silex\Validation;

final class NewsValidation
{
    public static function isValidNews(array &$post, array &$errors): bool
    {
        if (empty($post['title'])) {
            $errors[] = 'Empty title';
        }
        if (empty($post['content'])) {
            $errors[] = 'Empty message';
        }
        if (!empty($errors)) {
            return false;
        }
        if (strlen($post['title']) > 60) {
            $errors[] = 'Title too long';
        }
        $post['title'] = htmlspecialchars($post['title']);
        $post['content'] = htmlspecialchars($post['content']);
        return empty($errors);
    }

    public static function isValidDate(array &$get, array &$errors): bool
    {
        if(!isset($get['dateDeb']) || !isset($get['dateFin'])){
            return false;
        }
        if(strtotime($get['dateDeb']) === false){
            $errors[] = 'Date début invalide';
        }
        if(strtotime($get['dateFin']) === false){
            $errors[] = 'Date fin invalide';
        }
        if($get['dateDeb'] > $get['dateFin']){
            $errors[] = 'Date début supérieur à date fin';
        }
        return empty($errors);
    }
}
