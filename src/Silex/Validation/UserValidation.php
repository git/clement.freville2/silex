<?php

declare(strict_types=1);

namespace Silex\Validation;

final class UserValidation
{
    public static function isValidLogin(array $post, array &$errors): bool
    {
        self::isValidName($post, $errors);
        if(empty($post['password'])) {
            $errors[] = 'Password error';
        }
        return empty($errors);
    }

    public static function isValidUser(array $post, array &$errors): bool
    {
        self::isValidName($post, $errors);
        if(empty($post['password'])) {
            $errors[] = 'Password empty error';
        }

        if(empty($post['password-confirmation'])) {
            $errors[] = 'Password confirmation empty error';
        }

        if($post['password'] !== $post['password-confirmation']){
            $errors[] = 'Password confirmation not matching error';
        }

        return empty($errors);
    }

    public static function isValidName(array $post, array &$errors, string $key = 'login'): bool
    {
        if(empty($post[$key])) {
            $errors[] = 'Empty login';
        } else if(strlen($post[$key]) > 32) {
            $errors[] = 'Login too long';
        }
        return empty($errors);
    }
}
