<?php

declare(strict_types=1);

namespace Silex\Http;

use Silex\DI\DI;

class HttpResponse
{
    private int $status;

    private string $viewPath;

    private array $viewParams;

    public function __construct(int $status, string $viewPath, array $viewParams = [])
    {
        $this->status = $status;
        $this->viewPath = $viewPath;
        $this->viewParams = $viewParams;
    }

    public static function redirect(string $url): void
    {
        http_response_code(303);
        header('Location: ' . $url);
        exit();
    }

    public static function found(string $viewPath, array $viewParams = []): HttpResponse
    {
        return new HttpResponse(200, $viewPath, $viewParams);
    }

    public function render(DI $di, string $viewBasePath)
    {
        http_response_code($this->status);
        $router = $di->getRouter();
        $security = $di->getSecurity();
        $params = $this->viewParams;
        ob_start();
        require $viewBasePath . '/' . $this->viewPath . '.php';
        $content = ob_get_clean();
        require $viewBasePath . '/layout.php';
    }
}
