<?php

declare(strict_types=1);

namespace Silex\Router;

final class PathHelper
{
    public static function removeEverythingAfter(string $str, string $char): string
    {
        $pos = strpos($str, $char);
        if ($pos !== false) {
            return substr($str, 0, $pos);
        }
        return $str;
    }

    public static function startsWith(string $haystack, string $needle): bool
    {
        return strpos($haystack, $needle) === 0;
    }
}
