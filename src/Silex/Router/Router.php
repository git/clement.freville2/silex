<?php

declare(strict_types=1);

namespace Silex\Router;

use Silex\Controller\FrontController;
use Silex\Http\HttpResponse;
use Silex\DI\DI;

class Router
{
    private string $url;

    private string $basePath = '';

    /**
     * @var Route[]
     */
    private array $routes = [];

    public function __construct(string $url)
    {
        $url = PathHelper::removeEverythingAfter($url, '?');
        $url = PathHelper::removeEverythingAfter($url, '#');
        $this->url = trim($url, '/');
    }

    public function setBasePath(string $basePath)
    {
        $this->basePath = $basePath;
    }

    public function get(string $path, callable $callable): self
    {
        return $this->addRoute(['GET'], $path, $callable);
    }

    public function post(string $path, callable $callable): self
    {
        return $this->addRoute(['POST'], $path, $callable);
    }

    public function match(string $path, callable $callable): self
    {
        return $this->addRoute(['GET', 'POST'], $path, $callable);
    }

    public function delete(string $path, callable $callable): self
    {
        return $this->addRoute(['DELETE'], $path, $callable);
    }

    private function addRoute(array $methods, string $path, $callable): self
    {
        $route = new Route($path, $callable);
        foreach ($methods as $method) {
            $this->routes[$method][] = $route;
        }
        return $this;
    }

    public function url(string $url): string
    {
        if ($this->basePath !== '') {
            return "/" . $this->basePath . '/' . $url;
        } else {
            return $this->basePath . '/' . $url;
        }
    }

    public function run(DI $di): HttpResponse
    {
        if (!isset($this->routes[$_SERVER['REQUEST_METHOD']])) {
            return (new FrontController(null))->run($di);
        }
        $url = $this->url;
        if ($this->basePath !== '') {
            if (PathHelper::startsWith($url, $this->basePath)) {
                $url = trim(substr($url, strlen($this->basePath)), '/');
            } else {
                return (new FrontController(null))->run($di);
            }
        }
        foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route) {
            if ($route->matches($url)) {
                return (new FrontController($route))->run($di);
            }
        }
        return (new FrontController(null))->run($di);
    }
}
