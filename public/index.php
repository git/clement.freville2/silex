<?php

use Silex\Router\Router;

require __DIR__ . '/../src/Silex/Config/SplClassLoader.php';
require __DIR__ . '/../src/Silex/Config/Config.php';

$loader = new SplClassLoader('Silex', __DIR__ . '/../src');
$loader->register();

$security = new \Silex\Controller\SecurityController();
$visitor = new \Silex\Controller\VisitorController();
$admin = new \Silex\Controller\AdminController();
$router = new Router($_SERVER['REQUEST_URI']);
$router->get('/^$/', [$visitor, 'index']);
$router->get('/^recent\/(?<page>\d+)$/', [$visitor, 'index']);
$router->get('/^news\/(?<slug>[A-Za-z0-9-]+)-(?<id>\d+)$/', [$visitor, 'viewPost']);
$router->post('/^comment\/(?<id>\d+)$/', [$visitor, 'comment']);
$router->match('/^login$/', [$security, 'login']);
$router->match('/^register$/', [$security, 'register']);
$router->match('/^logout$/', [$security, 'logout']);
$router->match('/^admin\/publish$/', [$admin, 'publish']);
$router->match('/^admin\/edit\/(?<id>\d+)$/', [$admin, 'edit']);
$router->delete('/^admin\/delete\/(?<id>\d+)$/', [$admin, 'delete']);

$di = new \Silex\DI\DI($router);
$router->run($di)->render($di, __DIR__ . '/../' . VIEW_PATH);
