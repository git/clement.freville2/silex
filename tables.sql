CREATE TABLE registered_user (
    id_user SERIAL PRIMARY KEY,
    login VARCHAR(32) NOT NULL UNIQUE,
    password VARCHAR(72) NOT NULL, -- BCrypt
    role INT NOT NULL DEFAULT 0
);

CREATE TABLE news (
    id_news SERIAL PRIMARY KEY,
    publication_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    title VARCHAR(60) NOT NULL,
    slug VARCHAR(60) NOT NULL,
    content TEXT NOT NULL,
    author_id INT NOT NULL,
    FOREIGN KEY (author_id) REFERENCES registered_user(id_user)
        ON DELETE CASCADE
);
CREATE TABLE comment (
    id_comment SERIAL PRIMARY KEY,
    news_id INT NOT NULL,
    publication_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    content TEXT NOT NULL,
    author_id INT NULL,
    author_name VARCHAR(32) NULL,
    CONSTRAINT either_authors CHECK ((author_id IS NULL) != (author_name IS NULL)),
    FOREIGN KEY (news_id) REFERENCES news(id_news)
        ON DELETE CASCADE,
    FOREIGN KEY (author_id) REFERENCES registered_user(id_user)
        ON DELETE CASCADE
);
