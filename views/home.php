<?php require 'errors.php' ?>
<?php $params['title'] = 'Home';
$get = $_SERVER['QUERY_STRING'] ?? '';
if (!empty($get)) {
    $get = '?' . $get;
}
?>
<div>
    <p>
        <?= "Nombre de messages total : " . $params['nbComments'] ?> <br>
        <?= "Nombre de messages de l'utilisateur : " . $params['nbCommentsByUser'] ?>
    </p>
</div>
<hr>
<form method="get" style="display: grid; grid-template-columns: repeat(3, 1fr); grid-column-gap: 8px; align-items: center">
    <div class="field">
        <label class="label" for="dateDeb">From</label>
        <div class="control">
            <input class="input" type="date" id="dateDeb" name="dateDeb">
        </div>
    </div>
    <div class="field">
        <label class="label" for="dateFin">To</label>
        <div class="control">
            <input class="input" type="date" id="dateFin" name="dateFin">
        </div>
    </div>
    <div class="field">
        <div class="control">
            <input type="submit" class="button is-primary" value="Search">
        </div>
    </div>
</form>
<hr>
<?php foreach ($params['news'] as $news) : ?>
    <a href="<?= $router->url($news->getSlugRedirect()) ?>">
        <div class="card">
            <header class="card-header">
                <p class="card-header-title">
                    <?= $news->getPublicationDate()->format('Y-m-d') . " : " . $news->getTitle() ?>
                    <?php if ($security->getCurrentUser() !== null && $security->getCurrentUser()->isAdmin()): ?>
                        <a class="button ml-4" href="<?= $router->url("admin/edit/" . $news->getId())?>">Edit</a>
                        <button class="button is-danger ml-4" onclick="deleteReq(<?= $news->getId() ?>); return false;">Delete</button>
                    <?php endif; ?>
                </p>
            </header>
            <div class="card-content">
                <div class="content">
                    <?= $news->getContent() ?>...
                </div>
            </div>
        </div>
    </a>
<?php endforeach ?>
<hr>
<nav class="pagination" role="navigation" aria-label="pagination">
    <?php if ($params['page'] > 1) : ?>
        <a class="pagination-previous" href="<?= $router->url('recent/' . ($params['page'] - 1)) . $get ?>">Previous</a>
    <?php endif; ?>
    <?php if ($params['page'] < $params['nbPages']) : ?>
        <a class="pagination-next" href="<?= $router->url('recent/' . ($params['page'] + 1)) . $get ?>">Next page</a>
    <?php endif; ?>
    <ul class="pagination-list">
        <?php if ($params['page'] > 2) : ?>
            <li>
                <a class="pagination-link" aria-label="Goto page 1" href="<?= $router->url('recent/1') . $get ?>">1</a>
            </li>
            <?php if ($params['page'] > 3) : ?>
                <li>
                    <span class="pagination-ellipsis">&hellip;</span>
                </li>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($params['page'] > 1) : ?>
            <li>
                <a class="pagination-link" aria-label="Goto page <?= $params['page'] - 1 ?>" href="<?= $router->url('recent/' . ($params['page'] - 1)) . $get ?>"><?= $params['page'] - 1 ?></a>
            </li>
        <?php endif; ?>
        <li>
            <a class="pagination-link is-current" aria-label="Page <?= $params['page'] ?>" aria-current="page"><?= $params['page'] ?></a>
        </li>
        <?php if ($params['page'] < ($params['nbPages'] - 1)) : ?>
            <li>
                <a class="pagination-link" aria-label="Goto page <?= $params['page'] + 1 ?>" href="<?= $router->url('recent/' . ($params['page'] + 1)) . $get ?>"><?= $params['page'] + 1 ?></a>
            </li>
            <?php if ($params['page'] < ($params['nbPages'] - 2)) : ?>
                <li>
                    <span class="pagination-ellipsis">&hellip;</span>
                </li>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($params['page'] < $params['nbPages']) : ?>
            <li>
                <a class="pagination-link" aria-label="Goto page <?= $params['nbPages'] ?>" href="<?= $router->url('recent/' . $params['nbPages']) . $get ?>"><?= $params['nbPages'] ?></a>
            </li>
        <?php endif; ?>
    </ul>
</nav>

<?php if ($security->getCurrentUser() !== null && $security->getCurrentUser()->isAdmin()): ?>
<script>
    function deleteReq(i) {
        fetch(`<?= $router->url('admin/delete/') ?>${i}`, {
            method: 'DELETE'
        }).then(() => location.reload());
        return false;
    }
</script>
<?php endif; ?>
