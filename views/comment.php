<?php $params['title'] = 'Comment'; ?>
<h1>Comments</h1>
<?php foreach ($params['comments'] as $comment) : ?>
    <div class="card">
        <header class="card-header">
            <p class="card-header-title">
                <?= $comment->getCommentId() ?>
            </p>
        </header>
        <div class="card-content">
            <div class="content">
                <?= "AuthorId :" . $comment->getAuthorId() . " published on " . $comment->getPublicationDate()->format('Y-m-d H:i:s')  ?>
            </div>
            <div class="content">
                <?= $comment->getContent() ?>
            </div>
        </div>
    </div>
<?php endforeach ?>