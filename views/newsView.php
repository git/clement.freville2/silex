<?php
/**
 * @var \Silex\Model\News $news
 */
$news = $params['news'];
/**
 * @var \Silex\Model\Comment[] $comments
 */
$comments = $params['comments'];
$title = $news->getTitle();
?>
<h1>News</h1>
<div class="card">
    <header class="card-header">
        <p class="card-header-title">
            <?= $news->getTitle() ?>
        </p>
    </header>
    <div class="card-content">
        <div class="content">
            <?= $news->getContent() ?>
        </div>
    </div>
</div>

<section class="section comments">
    <?php foreach ($comments as $comment) : ?>
        <article class="message">
            <header class="message-header">
                From <?= $comment->getAuthorName() ?> published on <?= $comment->getPublicationDate()->format('Y-m-d H:i:s') ?>
            </header>
            <div class="message-body">
                <?= $comment->getContent() ?>
            </div>
        </article>
    <?php endforeach; ?>
    <form action="<?= $router->url('comment/' . $params['news']->getId()) ?>" method="post">
        <?php if ($security->getCurrentUserId() === null): ?>
            <div class="field">
                <label class="label" for="name">Name</label>
                <div class="control">
                    <input class="input" id="name" name="name" value="<?= $_SESSION['previous_name'] ?? '' ?>">
                </div>
            </div>
        <?php endif; ?>
        <div class="field">
            <label class="label" for="content">Comment</label>
            <div class="control">
                <textarea class="textarea" id="content" name="content"></textarea>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
        </div>
    </form>
</section>
