<?php require 'errors.php' ?>
<form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
    <div class="field">
        <label class="label" for="title">Title</label>
        <div class="control">
            <input class="input" type="text" id="title" name="title" value="<?= $params['news']->getTitle() ?>" required>
        </div>
    </div>
    <div class="field">
        <label class="label" for="content">Content</label>
        <div class="control">
            <textarea class="textarea" id="content" name="content" rows="10" required><?= $params['news']->getContent() ?></textarea>
        </div>
    </div>

    <div class="field">
        <div class="control">
            <button class="button is-link">Submit</button>
        </div>
    </div>
</form>
