<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?? 'Is it a blog?' ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
</head>
<body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="<?= $router->url('') ?>">Home</a>

                <?php if ($security->getCurrentUser() !== null && $security->getCurrentUser()->isAdmin()): ?>
                    <a class="navbar-item" href="<?= $router->url('admin/publish') ?>">
                        Publish
                    </a>
                <?php endif; ?>
            </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    <?php if ($security->getCurrentUser() !== null) : ?>
                        <p class="navbar-item"><?= $security->getCurrentUser()->getLogin() ?></p>
                    <?php endif ?>
                    <div class="buttons">
                        <?php if ($security->getCurrentUser() === null): ?>
                            <a class="button is-primary" href="<?= $router->url('register') ?>">
                                <strong>Sign up</strong>
                            </a>
                            <a class="button is-light" href="<?= $router->url('login') ?>">Log in</a>
                        <?php else : ?>
                            <a class="button is-light" href="<?= $router->url('logout') ?>">Log out</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <section class="section">
        <div class="container">
            <?= $content ?>
        </div>
    </section>
</body>

</html>